import factorial from './fact'


const ncr =(n:number, r:number):number=>{
  
  const ncrval=factorial(n)/(factorial(n-r)*factorial(r));
  return ncrval;
}
export default ncr;

