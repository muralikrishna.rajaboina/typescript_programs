import factorial from './fact';

test('factorial', () => {
  expect(factorial(5)).toEqual(120);
  expect(factorial(1)).toEqual(1);
  expect(factorial(2)).toEqual(2);
  expect(factorial(3)).toEqual(6);

});
console.log(factorial(6))