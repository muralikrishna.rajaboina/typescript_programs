import plus from './operators';

test('plus', () => {
  expect(plus(1, 2)).toEqual(4);
});
