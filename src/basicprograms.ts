// export and import programs

import { fname } from './exportB';
console.log(fname);

import plus from './operators';
console.log(plus(11, 22));

import factorial from './fact';
console.log(factorial(4));

import range from './range';
console.log(range(2, 7));

import ncr from './ncrvalue';
console.log(ncr(10, 4));

/*basic programs

1. returns the largest number in two numbers */

function max2(x: number, y: number): number {
  if (x > y) {
    return x;
  } else {
    return y;
  }
}
console.log(max2(2, 7));

// 2.returns the largest number in three numbers

function max3(x: number, y: number, z: number): number {
  const n = max2(x, y);
  return max2(n, z);
}
console.log(max3(2, 5, 8));

// 3.isFactor(n,i) returns true if i is factor of n else false

function isFactor(n: number, i: number): boolean {
  if (n % i === 0) {
    return true;
  }
  return false;
}
console.log(isFactor(20, 3));

// 4.isEven(n) returns true, if n is even, else false

function isEven(n: number): boolean {
  if (n % 2 === 0) {
    return true;
  }
  return false;
}
console.log(isEven(10));

// 5.isOdd(n) returns true, if n is odd, else false

function isOdd(n: number): boolean {
  if (n % 2 !== 0) {
    return true;
  }
  return false;
}
console.log(isOdd(22));

/* 6.liesBetween(n,x,y) returns true if n lies
    between x and y else false */

function liesBetween(n: number, x: number, y: number): boolean {
  if (n > x && n < y) {
    return true;
  }
  return false;
}
console.log(liesBetween(17, 15, 20));

// 7.isLeap(year) returns true if year is leap else false

function isLeap(year: number): boolean {
  if (year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0)) {
    return true;
  }
  return false;
}
console.log(isLeap(2012));

// 8.max(arr) returns largest number in arr

function max(arr: number[]): number {
  let c = 0;
  for (let i = 0; i <= arr.length; ++i) {
    if (c <= arr[i]) {
      c = arr[i];
    }
  }
  return c;
}
console.log(max([7, 55, 15, 43, 66, 13, 34]));

/* 9.suare All (arr:number):number
   ex [1,2,3]=>[1,4,9]  */

function squareAll(arr: number[]): number[] {
  const arr2 = [];
  for (let i = 0; i < arr.length; ++i) {
    arr[i] = arr[i] * arr[i];
    arr2.push(arr[i]);
  }
  return arr2;
}
console.log(squareAll([2, 4, 6]));

// 10. allEven[3,5,6,8,10]=>[6,8,10]
function allEven(arr: number[]): number[] {
  const arr2 = [];
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] % 2 === 0) {
      arr2.push(arr[i]);
    }
  }
  return arr2;
}
console.log(allEven([1, 2, 3, 4, 5, 6, 8, 9]));

// 11.concat([1,2,3],[4,5,6])=>[1,2,3,4,5,6]
function concat(arr1: number[], arr2: number[]): number[] {
  for (const i of arr2) {
    arr1.push(i);
  }
  return arr1;
}
console.log(concat([1, 2, 3], [55, 25, 9, 13]));

// 12 indexOf([1,2,34,5])

const indexof = (arr: number[], value: number): number => {
  for (let i = 0; i < arr.length; ++i) {
    if (arr[i] === value) {
      return i;
    }
  }
  return -1;
};
console.log(indexof([1, 3, 6, 19, 55], 19));

// 13. powerOf(x,y)
function power(x: number, y: number): number {
  if (y === 0) {
    return 1;
  } else {
    let z = 1;
    for (let i = 1; i <= y; ++i) {
      z = z * x;
    }

    return z;
  }
}
console.log(power(6, 2));

// 14.ncr(n,r)

function facto(n: number): number {
  let fact = 1;
  for (let i = 1; i <= n; ++i) {
    fact *= i;
  }
  return fact;
}
console.log(facto(5));

function ncrr(n: number, r: number): number {
  const z = facto(n) / (facto(n - r) * facto(r));
  return z;
}
console.log(ncrr(5, 3));
