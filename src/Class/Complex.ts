export default class Complex {
  real: number;
  imaginary: number;
  constructor(real: number, imaginary: number) {
    this.real = real;
    this.imaginary = imaginary;
  }

  equals = (that: Complex): boolean => {
    return this.real === that.real && this.imaginary === that.imaginary;
  };
  add = (r1: Complex): Complex => {
    const n = this.real + r1.real;
    const d = this.imaginary + r1.imaginary;
    return new Complex(n, d);
  };

  subtract = (r1: Complex): Complex => {
    const n = this.real - r1.real;
    const d = this.imaginary - r1.imaginary;
    return new Complex(n, d);
  };

  multiply = (r1: Complex): Complex => {
    const n = this.real * r1.real + this.imaginary * r1.imaginary * -1;
    const d = this.imaginary * r1.real + r1.imaginary * this.real;
    return new Complex(n, d);
  };

  divid = (r1: Complex): Complex => {
    const n =
      (this.real * r1.real + this.imaginary * r1.imaginary) /
      (r1.real * r1.real + r1.imaginary * r1.imaginary);
    const d =
      (this.imaginary * r1.real + r1.imaginary * this.real * -1) /
      (r1.real * r1.real + r1.imaginary * r1.imaginary);
    return new Complex(n, d);
  };
  compare = (r1: Complex): number => {
    return this.real * r1.imaginary - r1.real * this.imaginary;
  };
  less = (r: Complex): boolean => {
    return this.compare(r) < 0;
  };
  equal = (r: Complex): boolean => {
    return this.compare(r) === 0;
  };
  greater = (r: Complex): boolean => {
    return this.compare(r) > 0;
  };
}
