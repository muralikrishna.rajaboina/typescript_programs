import Complex from './Complex';

test('constructor', () => {
  const r = new Complex(1, 2);
  expect(r.real).toEqual(1);
  expect(r.imaginary).toEqual(2);
});
test('constructor', () => {
  const r = new Complex(2, 4);
  expect(r.real).toEqual(1);
  expect(r.imaginary).toEqual(2);
});

test('equals', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(3, 6);
  const r3 = new Complex(1, 3);
  expect(r.equals(r2)).toBeTruthy();
  expect(r.equals(r3)).toBeFalsy();
});

test('add', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(2, 3);
  const expected = new Complex(7, 6);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('subtract', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(3, 2);
  const expected = new Complex(-4, 4);
  const r3 = r.subtract(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('multiply', () => {
  const r = new Complex(1, 4);
  const r2 = new Complex(2, 3);
  const expected = new Complex(-10, 11);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('division', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(2, 3);
  const expected = new Complex(1.6, -0.2);
  const r3 = r.divid(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('compare', () => {
  const r = new Complex(9, 16);
  const r2 = new Complex(5, 16);
  const r3 = r.equal(r2);
  expect(r3).toBeFalsy();
});
test('compare', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(2, 3);
  const r3 = r.greater(r2);
  expect(r3).toBeFalsy();
});

test('compare', () => {
  const r = new Complex(1, 2);
  const r2 = new Complex(2, 3);
  const r3 = r.less(r2);
  expect(r3).toBeTruthy();
});
