const gcd = (x: number, y: number): number => {
  if (!y) {
    return x;
  }
  return gcd(y, x % y);
};
console.log(gcd(2, 4));

export default class Rational {
  readonly numerator: number;
  readonly denominator: number;
  constructor(numerator = 0, denominator = 1) {
    const g = gcd(numerator, denominator);
    this.numerator = numerator / g;
    this.denominator = denominator / g;
  }
  equals = (that: Rational): boolean => {
    return (
      this.numerator === that.numerator && this.denominator === that.denominator
    );
  };
  add = (r: Rational): Rational => {
    const n = this.numerator * r.denominator + this.denominator * r.numerator;
    const d = this.denominator * r.denominator;
    return new Rational(n, d);
  };

  subtract = (r: Rational): Rational => {
    const n = this.numerator * r.denominator - this.denominator * r.numerator;
    const d = this.denominator * r.denominator;
    return new Rational(n, d);
  };
  multiply = (r: Rational): Rational => {
    const n = this.numerator * r.denominator * this.denominator * r.numerator;
    const d = this.denominator * r.denominator;
    return new Rational(n, d);
  };
  division = (r: Rational): Rational => {
    const n = this.numerator * r.denominator / r.numerator * this.denominator;
    const d = this.denominator * r.denominator;
    return new Rational(n, d);
  };
  compare = (r1: Rational): number => {
    return this.numerator * r1.denominator - this.denominator * r1.numerator;
  };
  less = (r: Rational): boolean => {
    return this.compare(r) < 0;
  };
  equal = (r: Rational): boolean => {
    return this.compare(r) === 0;
  };
  greater = (r: Rational): boolean => {
    return this.compare(r) > 0;
  };
}
