import Rational from './Rational';
test('constructor', () => {
  const r = new Rational(1, 2);
  expect(r.numerator).toEqual(1);
  expect(r.denominator).toEqual(2);
});
test('constructor', () => {
  const r = new Rational(2, 4);
  expect(r.numerator).toEqual(1);
  expect(r.denominator).toEqual(2);
});

test('equals', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(3, 6);
  const r3 = new Rational(1, 3);
  expect(r.equals(r2)).toBeTruthy();
  expect(r.equals(r3)).toBeFalsy();
});

test('add', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const expected = new Rational(7, 6);
  const r3 = r.add(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('subtract', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(3, 2);
  const expected = new Rational(-4, 4);
  const r3 = r.subtract(r2);
  expect(r3.equals(expected)).toBeTruthy();
});
test('multiply', () => {
  const r = new Rational(1, 4);
  const r2 = new Rational(2, 3);
  const expected = new Rational(2, 1);
  const r3 = r.multiply(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('division', () => {
  const r = new Rational(3, 6);
  const r2 = new Rational(5, 4);
  const expected = new Rational(12, 18);
  const r3 = r.division(r2);
  expect(r3.equals(expected)).toBeTruthy();
});

test('equal', () => {
  const r = new Rational(2, 3);
  const r2 = new Rational(2, 3);
  const r3 = r.equal(r2);
  expect(r3).toBeTruthy();
});
test('compare', () => {
  const r = new Rational(3, 1);
  const r2 = new Rational(5, 6);
  const r3 = r.equal(r2);
  expect(r3).toBeFalsy();
});
test('compare', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const r3 = r.greater(r2);
  expect(r3).toBeFalsy();
});

test('compare', () => {
  const r = new Rational(1, 2);
  const r2 = new Rational(2, 3);
  const r3 = r.less(r2);
  expect(r3).toBeTruthy();
});
