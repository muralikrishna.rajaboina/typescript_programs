var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var products = [{ id: 1, name: 'murali' }, { id: 2, name: 'kalyan' }];
var currentId = 2;

var PORT = process.env.PORT || 2018;
app.use(express.static(__dirname));
app.use(bodyParser.json());
app.get('/products', function(req, res) {
  res.send({ products: products });
});
app.post('/products', function(req, res) {
  var productName = req.body.name;
  currentId++;
  products.push({
    id: currentId,
    name: productName
  });
  res.send('successfully created..');
});
app.put('products/:id', function(req, res) {
  var id = req.params.id;
  var newName = req.body.newName;
  var found = false;
  products.forEach(function(product, index) {
    if (!found && product.id === Number(id)) {
      product.name = newName;
    }
  });
  res.send('successfully updated..');
});
app.delete('products/:id', function(req, res) {
  var found = false;
  products.forEach(function(product, index) {
    if (!found && product.id === Number(id)) {
      product.splice(index, 1);
    }
  });
  res.send('successfully deleted');
});
app.listen(PORT, function() {
  console.log('server listening ' + PORT);
});
