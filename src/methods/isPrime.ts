const factors1 = (n: number): number[] => range1(1, n).filter(i => n % i === 0);
console.log(factors(24));

const isPrime = (n: number): boolean =>
  factors(n).length === 2 ? true : false;
console.log(isPrime(7));
