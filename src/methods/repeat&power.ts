const range3 = (start: number, stop: number): number[] => {
  const result: number[] = [];
  for (let i = start; i <= stop; i++) {
    result.push(i);
  }
  return result;
};

console.log(range3(2, 7));

const repeat = (x: number, n: number): ReadonlyArray<number> =>
  range3(1, n).map(i => (i = x));
console.log(repeat(4, 12));

const power = (x: number, n: number): number =>
  repeat(x, n).reduce((a, b) => a * b);
console.log(power(4, 2));
