// map, reduce, filter methods in type script

// Define the callback function.

const arr = [10, 20, 30];
const square = (x: number): number => x * x;
console.log(arr.map(square));

const arr1 = [10, 20, 24, 34, 45, 63, 30];
const lesY = (x: number): boolean => x < 50;
console.log(arr1.filter(lesY));

const arr2 = [10, 20, 24, 34, 45, 63, 30];
const red = (x: number, i: number): number => x + i;
console.log(arr2.reduce(red));

const product = (arr3: number[]): number => arr3.reduce((x, y) => x * y, 1);
console.log(product([1, 2, 3, 4, 5]));

const range1 = (start: number, stop: number): number[] => {
  const result: number[] = [];
  for (let i = start; i <= stop; i++) {
    result.push(i);
  }
  return result;
};

console.log(range1(2, 7));

const factors = (n: number): number[] => range1(1, n).filter(i => n % i === 0);
console.log(factors(24));

const isPerfect = (n: number) =>
  factors(n).reduce((x, y) => x + y) === 2 * n ? true : false;

console.log(isPerfect(28));

const perfectNumbers = (start: number, stop: number) =>
  range1(start, stop).filter(n => isPerfect(n));
console.log(perfectNumbers(1, 100));

const isPrime = (n: number): boolean =>
  factors(n).length === 2 ? true : false;
console.log(isPrime(7));

const primeNumbers = (start: number, stop: number) =>
  range1(start, stop).filter(n => isPrime(n));
console.log(primeNumbers(1, 30));

const firstName = 'murali';
const lastName = 'krishna';

const person = {
  firstName,
  lastName
};

console.log(person.firstName);
console.log(person.lastName);

const r = {
  height: 10,
  width: 20
};

const Area = () => {
  return r.height * r.width;
};
const perimeter = () => {
  return 2 * (r.height + r.width);
};
console.log(Area());
console.log(perimeter());

class Point {
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}
const pt: Point = new Point(1, 2);
console.log(pt.x, pt.y);
const perimeter1 = (x: number, y: number) => {
  return 2 * (x + y);
};

const Area1 = (x: number, y: number) => {
  return x * y;
};

console.log(perimeter1(pt.x, pt.y));
console.log(Area1(pt.x, pt.y));

const repeat = (x: number, n: number): ReadonlyArray<number> =>
  range1(1, n).map(i => (i = x));
console.log(repeat(4, 12));

const power = (x: number, n: number): number =>
  repeat(x, n).reduce((a, b) => a * b);
console.log(power(4, 2));

const factorial = (n: number): number =>
  range1(1, n).reduce((x, y) => x * y, 1);
console.log(factorial(6));

const ncr = (n: number, r: number): number =>
  factorial(n) / (factorial(r) * factorial(n - r));
console.log(ncr(10, 3));

const pascalLine = (n: number): number[] => range1(0, n).map(r => ncr(n, r));

console.log(pascalLine(3));

const pascalTriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> =>
  range1(0, lines - 1).map(line => pascalLine(line));

console.log(pascalTriangle(3));

const maxNum = (arr5: number[]): number =>
  arr5.reduce((a, b) => (a < b ? b : a));
console.log(maxNum([2, 5, 7, 45, 38, 59, 33]));
