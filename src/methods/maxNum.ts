const maxNum = (arr5: number[]): number =>
  arr5.reduce((a, b) => (a < b ? b : a));
console.log(maxNum([2, 5, 7, 45, 38, 59, 33]));
