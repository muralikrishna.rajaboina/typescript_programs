const factors1 = (n: number): number[] => range1(1, n).filter(i => n % i === 0);
console.log(factors(24));

const isPerfect = (n: number) =>
  factors1(n).reduce((x, y) => x + y) === 2 * n ? true : false;

console.log(isPerfect(28));
