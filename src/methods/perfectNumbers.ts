const isPerfect = (n: number) =>
  factors(n).reduce((x, y) => x + y) === 2 * n ? true : false;

console.log(isPerfect(28));

const perfectNumbers = (start: number, stop: number) =>
  range1(start, stop).filter(n => isPerfect(n));
console.log(perfectNumbers(1, 100));
