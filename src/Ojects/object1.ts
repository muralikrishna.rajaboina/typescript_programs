const firstName = 'murali';
const lastName = 'krishna';

const person = {
  firstName,
  lastName
};

console.log(person.firstName);
console.log(person.lastName);

const r = {
  height: 10,
  width: 20
};

const Area = () => {
  return r.height * r.width;
};
const perimeter = () => {
  return 2 * (r.height + r.width);
};
console.log(Area());
console.log(perimeter());

class Point {
  x: number;
  y: number;
  constructor(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}
const pt: Point = new Point(1, 2);
console.log(pt.x, pt.y);
const perimeter1 = (x: number, y: number) => {
  return 2 * (x + y);
};

const Area1 = (x: number, y: number) => {
  return x * y;
};

console.log(perimeter1(pt.x, pt.y));
console.log(Area1(pt.x, pt.y));
