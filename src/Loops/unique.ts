const unique = (arr: ReadonlyArray<number>): ReadonlyArray<number> => {
  const result = [];
  let temp = 0;
  for (let i = 0; i < arr.length; i++) {
    temp++;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[i] === arr[j]) {
        temp++;
      }
    }
    if (temp === 1) {
      result.push(arr[i]);
    }
    temp = 0;
  }
  return result;
};
console.log(unique([1, 3, 5, 7, 4, 1, 2, 1, 7, 5, 3]));
