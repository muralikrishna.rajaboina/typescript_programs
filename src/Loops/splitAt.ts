const splitAt = (arr: ReadonlyArray<number>, n: number) => {
  const first: number[] = [];

  for (let i = 0; i < n; ++i) {
    first.push(arr[i]);
  }
  const second: number[] = [];
  for (let i = n; i < arr.length; ++i) {
    second.push(arr[i]);
  }

  return [first, second];
};
console.log(splitAt([1, 2, 3, 4, 5, 6, 7], 3));
