const zip = (arr11: ReadonlyArray<number>, arr12: ReadonlyArray<number>) => {
  const result = [];

  for (let i = 0; i < arr11.length; ++i) {
    result.push([arr11[i], arr12[i]]);
  }
  return result;
};

console.log(zip([1, 2, 3, 4], [5, 6, 7, 8]));
