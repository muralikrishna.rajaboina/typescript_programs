const push = (arr9: ReadonlyArray<number>, arr10: ReadonlyArray<number>) => [
  ...arr9,
  ...arr10
];

console.log(push([55, 25, 9, 13], [2, 4, 7, 8]));
