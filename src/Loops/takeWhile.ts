const takeWhile = (arr6: number[], isEven: (n: number) => boolean) => {
  const result: number[] = [];
  for (const e of arr6) {
    if (isEven(e)) {
      result.push(e);
    } else {
      break;
    }
  }
  return result;
};
console.log(takeWhile([2, 4, 5, 6, 3, 12, 11], x => x <= 5));
