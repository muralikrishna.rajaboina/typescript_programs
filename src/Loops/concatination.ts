const concatination = (
  arr7: ReadonlyArray<number>,
  arr8: ReadonlyArray<number>
): ReadonlyArray<number> => {
  for (const e of arr8) {
    arr7 = [...arr7, e];
  }

  return arr7;
};
console.log(concatination([55, 25, 9, 13], [2, 4, 7, 8]));
